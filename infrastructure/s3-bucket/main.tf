# Specify the provider and access details
provider "aws" {
  region  = "${var.aws_region}"
  profile = "${var.key_name}"
}

resource "aws_s3_bucket" "video_bucket" {
  bucket = "webstream-video-store"
  acl    = "public-read-write"
  region = "${var.aws_region}"
  tags = {
    Name        = "Webstream Video Store"
    Environment = "Prod"
  }
  cors_rule {
    allowed_methods = ["GET", "HEAD", "POST"],
    allowed_origins = ["*"],
    allowed_headers = ["*"]
    max_age_seconds = 3000
  }
}
