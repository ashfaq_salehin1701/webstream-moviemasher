variable "key_name" {
  description = "Desired name of AWS key pair"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "ap-south-1"
}

# Ubuntu 18.04 LTS (x64)
variable "aws_amis" {
  default = {
    eu-west-1 = "ami-00035f41c82244dab"
    us-east-1 = "ami-0ac019f4fcb7cb7e6"
    us-east-2 = "ami-0f65671a86f061fcd"
    us-west-1 = "ami-063aa838bd7631e0b"
    us-west-2 = "ami-0bbe6b35405ecebdb"
  }
}
