resource "aws_appautoscaling_target" "moviemasher-rb-target" {
  max_capacity       = "${var.scale_upper_bound}"
  min_capacity       = "${var.scale_lower_bound}"
  resource_id        = "service/${aws_ecs_cluster.cluster.name}/mmrb-ecs"
  role_arn           = "${aws_iam_role.ecs_tasks_execution_role.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "moviemasher_rb_scale_up" {
  depends_on         = ["aws_appautoscaling_target.moviemasher-rb-target"]
  name               = "moviemasher-rb-scale-up-queue"
  resource_id        = "service/${aws_ecs_cluster.cluster.name}/mmrb-ecs"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  step_scaling_policy_configuration = {
    cooldown                = "${var.scale_cooldown_time}"
    adjustment_type         = "ChangeInCapacity"
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = "${var.scale_up_step}"
    }
  }
}

resource "aws_appautoscaling_policy" "moviemasher_rb_scale_down" {
  depends_on         = ["aws_appautoscaling_target.moviemasher-rb-target"]
  name               = "moviemasher-rb-scale-down-queue"
  resource_id        = "service/${aws_ecs_cluster.cluster.name}/mmrb-ecs"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"

  step_scaling_policy_configuration = {
    cooldown                = "${var.scale_cooldown_time}"
    adjustment_type         = "ChangeInCapacity"
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = "${var.scale_down_step}"
    }
  }
}

##
## Cloudwatch Alarms
##
resource "aws_cloudwatch_metric_alarm" "service_queue_high" {
  alarm_name          = "moviemasher-rb-queue-count-high"
  alarm_description   = "This alarm monitors ${var.sqs_queue_name} Queue count utilization for scaling up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"
  period              = "60"
  statistic           = "Average"
  threshold           = "${var.queue_high_threshold}"
  alarm_actions       = ["${aws_appautoscaling_policy.moviemasher_rb_scale_up.arn}"]

  dimensions {
    QueueName = "${var.sqs_queue_name}"
  }
}

# A CloudWatch alarm that monitors CPU utilization of containers for scaling down
resource "aws_cloudwatch_metric_alarm" "service_queue_low" {
  alarm_name          = "moviemasher-rb-queue-count-low"
  alarm_description   = "This alarm monitors ${var.sqs_queue_name} Queue count utilization for scaling down"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"
  period              = "60"
  statistic           = "Average"
  threshold           = "${var.queue_low_threshold}"
  alarm_actions       = ["${aws_appautoscaling_policy.moviemasher_rb_scale_down.arn}"]

  dimensions {
    QueueName = "${var.sqs_queue_name}"
  }
}