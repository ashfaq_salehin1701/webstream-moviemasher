data "template_file" "hello-world" {
  template = "${file("${path.root}/task-definition/hello-world.json")}"
}

resource "aws_security_group" "bastion_sg" {
  name   = "bastion-cluster-sg"
  vpc_id = "${module.amm-vpc.vpc_id}"

  ingress {
    protocol  = "-1"
    from_port = 0
    to_port   = 0

    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name        = "bastion-ecs-cluster-sg"
  }
}

module "bastion-ecs" {
  source = "npalm/ecs-service/aws"

  service_name          = "bastion-ecs"
  service_desired_count = 1

  environment = "bastion-host"
  task_definition = "${data.template_file.hello-world.rendered}"
  vpc_id       = "${module.amm-vpc.vpc_id}"
  vpc_cidr     = "${module.amm-vpc.vpc_cidr_block}"
  enable_lb    = 0

  ecs_cluster_id = "${aws_ecs_cluster.cluster.id}"

  task_cpu        = "1024"
  task_memory     = "2048"

  service_launch_type = "EC2"

  awsvpc_task_execution_role_arn = "${aws_iam_role.ecs_tasks_execution_role.arn}"
  awsvpc_service_security_groups = ["${aws_security_group.bastion_sg.id}"]
  awsvpc_service_subnetids       = ["${module.amm-vpc.public_subnets[2]}", "${module.amm-vpc.public_subnets[3]}"]
}

module "bastion_instance" {
  source  = "npalm/ecs-instances/aws"
  version = "0.3.0"

  ecs_cluster_name = "${aws_ecs_cluster.cluster.name}"
  aws_region       = "${var.aws_region}"
  environment      = "bastion-host"
  key_name         = "${var.key_name}"
  vpc_id           = "${module.amm-vpc.vpc_id}"
  vpc_cidr         = "0.0.0.0/0"
  subnets          = ["${module.amm-vpc.public_subnets[2]}", "${module.amm-vpc.public_subnets[3]}"]
  instance_type    = "${var.instance_type}"
}