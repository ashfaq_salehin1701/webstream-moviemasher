locals {
  amm_ecs_container_name = "angular-moviemasher"
  amm_ecs_container_port = "80"
}

data "template_file" "amm_ecs" {
  template = "${file("${path.root}/task-definition/angular-moviemasher.json")}"

  vars {
    image                 = "${var.angular-moviemasher-image}"
    aws_access_key_id     = "${var.aws_access_key_id}"
    aws_secret_access_key = "${var.aws_secret_access_key}"
    s3_bucket             = "${var.s3_bucket}"
    s3_region             = "${var.s3_region}"
    user_media_protocol   = "${var.user_media_protocol}"
    user_media_host       = "${var.user_media_host}"
    sqs_queue_url         = "${var.sqs_queue_url}"
    aws_region            = "${var.aws_region}"
    log_group_name   = "${aws_cloudwatch_log_group.log_group.name}"
    log_group_region = "${var.aws_region}"
    log_group_prefix = "angular-moviemasher"
  }
}

module "amm-ecs" {
  source = "npalm/ecs-service/aws"

  service_name          = "amm-ecs"
  service_desired_count = 1

  environment = "${var.environment-amm}"

  vpc_id       = "${module.amm-vpc.vpc_id}"
  vpc_cidr     = "${module.amm-vpc.vpc_cidr_block}"
  lb_subnetids = ["${module.amm-vpc.public_subnets[0]}", "${module.amm-vpc.public_subnets[1]}"]

  ecs_cluster_id = "${aws_ecs_cluster.cluster.id}"

  lb_internal = false

  task_definition = "${data.template_file.amm_ecs.rendered}"
  task_cpu        = "1024"
  task_memory     = "2048"

  service_launch_type = "EC2"

  awsvpc_task_execution_role_arn = "${aws_iam_role.ecs_tasks_execution_role.arn}"
  awsvpc_service_security_groups = ["${aws_security_group.awsvpc_sg.id}"]
  awsvpc_service_subnetids       = ["${module.amm-vpc.private_subnets[0]}", "${module.amm-vpc.private_subnets[1]}"]

  lb_target_group = {
    container_name = "${local.amm_ecs_container_name}"
    container_port = "${local.amm_ecs_container_port}"
  }

  lb_listener = {
    port     = 80
    protocol = "HTTP"
  }
}
