locals {
  ecs_container_name = "angular-moviemasher"
  ecs_container_port = "80"
}

data "template_file" "mmrb_ecs" {
  template = "${file("${path.root}/task-definition/moviemasher-rb.json")}"

  vars {
    image                 = "${var.moviemasher-rb-image}"
    aws_access_key_id     = "${var.aws_access_key_id}"
    aws_secret_access_key = "${var.aws_secret_access_key}"
    sqs_queue_url         = "${var.sqs_queue_url}"
    aws_region            = "${var.aws_region}"
    log_group_name   = "${aws_cloudwatch_log_group.log_group.name}"
    log_group_region = "${var.aws_region}"
    log_group_prefix = "moviemasher-rb"
  }
}

module "mmrb-ecs" {
  source = "npalm/ecs-service/aws"

  service_name          = "mmrb-ecs"
  service_desired_count = 1

  environment = "${var.environment-amm}"

  vpc_id       = "${module.amm-vpc.vpc_id}"
  vpc_cidr     = "${module.amm-vpc.vpc_cidr_block}"
  enable_lb    = 0

  ecs_cluster_id = "${aws_ecs_cluster.cluster.id}"

  task_definition = "${data.template_file.mmrb_ecs.rendered}"
  task_cpu        = "1024"
  task_memory     = "2048"

  service_launch_type = "EC2"

  awsvpc_task_execution_role_arn = "${aws_iam_role.ecs_tasks_execution_role.arn}"
  awsvpc_service_security_groups = ["${aws_security_group.awsvpc_sg.id}"]
  awsvpc_service_subnetids       = ["${module.amm-vpc.private_subnets[2]}", "${module.amm-vpc.private_subnets[3]}"]
}
