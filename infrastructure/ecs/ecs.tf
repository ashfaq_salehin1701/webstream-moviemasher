resource "aws_key_pair" "key" {
  key_name   = "${var.key_name}"
  public_key = "${file("${var.ssh_key_file_ecs}")}"
}

module "amm_ecs_instances" {
  source  = "npalm/ecs-instances/aws"
  version = "0.3.0"

  ecs_cluster_name = "${aws_ecs_cluster.cluster.name}"
  aws_region       = "${var.aws_region}"
  environment      = "${var.environment-amm}"
  key_name         = "${var.key_name}"
  vpc_id           = "${module.amm-vpc.vpc_id}"
  vpc_cidr         = "${module.amm-vpc.vpc_cidr_block}"
  subnets          = ["${module.amm-vpc.private_subnets[0]}", "${module.amm-vpc.private_subnets[1]}"]
  instance_type    = "${var.instance_type}"
}

module "mmrb_ecs_instances" {
  source  = "npalm/ecs-instances/aws"
  version = "0.3.0"

  ecs_cluster_name = "${aws_ecs_cluster.cluster.name}"
  aws_region       = "${var.aws_region}"
  environment      = "${var.environment-mrb}"
  key_name         = "${var.key_name}"
  vpc_id           = "${module.amm-vpc.vpc_id}"
  vpc_cidr         = "${module.amm-vpc.vpc_cidr_block}"
  subnets          = ["${module.amm-vpc.private_subnets[2]}", "${module.amm-vpc.private_subnets[3]}"]
  instance_type    = "${var.instance_type}"
}