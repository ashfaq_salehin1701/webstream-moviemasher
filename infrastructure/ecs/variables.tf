variable "aws_region" {
  description = "The Amazon region"
  type        = "string"
  default     = "ap-south-1"
}

variable "environment-amm" {
  type    = "string"
  default = "module-amm"
}

variable "environment-mrb" {
  type    = "string"
  default = "module-mrb"
}

variable "instance_type" {
  type        = "string",
  description = "EC2 instance type to use"
  default     = "t2.medium"
}

variable "ssh_key_file_ecs" {
  default = "~/.ssh/id_rsa.pub"
}

variable "key_name" {
  type    = "string"
}

variable "angular-moviemasher-image" {
  description = "Docker image name"
}

variable "moviemasher-rb-image" {
  description = "Docker image name for moviemasher rb"
  default = "moviemasher/moviemasher.rb"
}

variable "aws_access_key_id" {
  description = "AWS access key id"
}

variable "aws_secret_access_key" {
  description = "AWS secret access key"
}

variable "s3_bucket" {
  description = "S3 Bucket name"
}

variable "s3_region" {
  description = "S3 Region"
}

variable "user_media_protocol" {
  description = "User media protocol"
}

variable "user_media_host" {
  description = "User media host"
}

variable "sqs_queue_url" {
  description = "SQS queue URL"
}

variable "sqs_queue_name" {
  description = "SQS queue name"
}

variable "queue_high_threshold" {
  description = "Moviemasher rb max capacity"
  default     = "5"
}

variable "queue_low_threshold" {
  description = "Moviemasher rb min capacity"
  default     = "3"
}

variable "scale_cooldown_time" {
  description = "Scale up or down cooldown time"
  default = "60"
}

variable "scale_lower_bound" {
  description = "Scale down lower bound"
  default = "2"
}

variable "scale_upper_bound" {
  description = "Scale up upper bound"
  default = "20"
}

variable "scale_up_step" {
  description = "Step for each scaling up"
  default = "1"
}

variable "scale_down_step" {
  description = "Step for each scaling down"
  default = "-1"
}

# Ubuntu 18.04 LTS (x64)
variable "aws_amis" {
  type    = "map"
  default = {
    eu-west-1  = "ami-00035f41c82244dab"
    us-east-1  = "ami-0ac019f4fcb7cb7e6"
    us-east-2  = "ami-0f65671a86f061fcd"
    us-west-1  = "ami-063aa838bd7631e0b"
    us-west-2  = "ami-0bbe6b35405ecebdb"
    ap-south-1 = "ami-01d6dff2590762a96"
  }
}