provider "aws" {
  region  = "${var.aws_region}"
}

module "amm-vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "my-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["ap-south-1a", "ap-south-1b"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24", "10.0.4.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24", "10.0.104.0/24"]

  enable_nat_gateway      = true
  enable_vpn_gateway      = false

  tags = {
    Environment = "${var.environment-amm}"
  }
}

resource "aws_cloudwatch_log_group" "log_group" {
  name = "moviemasher-${var.environment-amm}"
}

resource "aws_ecs_cluster" "cluster" {
  name = "${var.environment-amm}-ecs-cluster"
}

resource "aws_security_group" "awsvpc_sg" {
  name   = "${var.environment-amm}-awsvpc-cluster-sg"
  vpc_id = "${module.amm-vpc.vpc_id}"

  ingress {
    protocol  = "tcp"
    from_port = 0
    to_port   = 65535

    cidr_blocks = [
      "${module.amm-vpc.vpc_cidr_block}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name        = "${var.environment-amm}-ecs-cluster-sg"
    Environment = "${var.environment-amm}"
  }
}
