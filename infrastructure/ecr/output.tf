output "arn" {
  value="${aws_ecr_repository.webstream-moviemasher.arn}"
}
output "name" {
  value="${aws_ecr_repository.webstream-moviemasher.name}"
}
output "registry_id" {
  value="${aws_ecr_repository.webstream-moviemasher.registry_id}"
}
output "repository_url" {
  value="${aws_ecr_repository.webstream-moviemasher.repository_url}"
}



