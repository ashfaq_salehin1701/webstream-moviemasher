# Specify the provider and access details
provider "aws" {
  region  = "${var.aws_region}"
  profile = "${var.key_name}"
}

resource "aws_ecr_repository" "webstream-moviemasher" {
  name = "webstream/moviemasher"
}
