variable "public_key_path" {
  description = <<DESCRIPTION
Path to the SSH public key to be used for authentication.
Ensure this keypair is added to your local SSH agent so provisioners can
connect.

Example: ~/.ssh/terraform.pub
DESCRIPTION
}

variable "key_name" {
  description = "Desired name of AWS key pair"
}

variable "aws_account_id" {
  description = "Account id for AWS"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "ap-south-1"
}

variable "angular_moviemasher_public_subnet_cidrs" {
  description = "CIDR for public subnet"
  type = "list"
  default = ["10.0.1.0/24", "10.0.2.0/24"]
}

variable "angular_moviemasher_private_subnet_cidrs" {
  description = "CIDR for private subnet"
  type = "list"
  default = ["10.0.3.0/24", "10.0.4.0/24"]
}

variable "moviemasher_rb_public_subnet_cidrs" {
  description = "CIDR for public subnet"
  type = "list"
  default = ["10.0.5.0/24", "10.0.6.0/24"]
}

variable "moviemasher_rb_private_subnet_cidrs" {
  description = "CIDR for private subnet"
  type = "list"
  default = ["10.0.7.0/24", "10.0.8.0/24"]
}

variable "moviemasher_rb_autoscaling_private_subnet_cidrs" {
  description = "CIDR for private subnet"
  type = "list"
  default = ["10.0.9.0/24", "10.0.10.0/24"]
}

variable "moviemasher_rb_autoscaling_public_subnet_cidrs" {
  description = "CIDR for private subnet"
  type = "list"
  default = ["10.0.11.0/24", "10.0.12.0/24"]
}


variable "availability_zones" {
  description = "Availability zone 1"
  type    = "list"
  default = ["ap-south-1a", "ap-south-1b"]
}

variable "aws_access_key_id" {
  description = "AWS access key id"
}

variable "angular-moviemasher-image" {
  description = "Docker image name"
}

variable "moviemasher-rb-image" {
  description = "Docker image name for moviemasher rb"
  default = "moviemasher/moviemasher.rb"
}

variable "aws_secret_access_key" {
  description = "AWS secret access key"
}

variable "s3_bucket" {
  description = "S3 Bucket name"
}

variable "s3_region" {
  description = "S3 Region"
}

variable "user_media_protocol" {
  description = "User media protocol"
}

variable "user_media_host" {
  description = "User media host"
}

variable "sqs_queue_url" {
  description = "SQS queue URL"
}

variable "sqs_queue_name" {
  description = "SQS queue name"
}

variable "instance_type" {
  description = "Type of AWS instance to use"
  default = "t2.micro"
}

variable "queue_high_threshold" {
  description = "Moviemasher rb max capacity"
  default     = "2"
}

# Ubuntu 18.04 LTS (x64)
variable "aws_amis" {
  type    = "map"
  default = {
    eu-west-1  = "ami-00035f41c82244dab"
    us-east-1  = "ami-0ac019f4fcb7cb7e6"
    us-east-2  = "ami-0f65671a86f061fcd"
    us-west-1  = "ami-063aa838bd7631e0b"
    us-west-2  = "ami-0bbe6b35405ecebdb"
    ap-south-1 = "ami-01d6dff2590762a96"
  }
}
