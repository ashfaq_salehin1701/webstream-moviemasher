resource "aws_alb_target_group" "angular-moviemasher-group" {
  name        = "angular-moviemasher-group"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = "${aws_vpc.main.id}"
  target_type = "ip"
  depends_on  = ["aws_alb.angular-moviemasher"]
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_alb" "angular-moviemasher" {
  name            = "angular-moviemasher-elb"
  security_groups = ["${aws_security_group.load_balancers.id}"]
  subnets         = ["${aws_subnet.angular_moviemasher_public_subnet.*.id}"]

  tags {
    Name        = "angular-moviemasher-elb"
    Environment = "Production"
  }
}

resource "aws_alb_listener" "angular-moviemasher-listener" {
  load_balancer_arn = "${aws_alb.angular-moviemasher.arn}"
  port              = "80"
  protocol          = "HTTP"
  depends_on        = ["aws_alb_target_group.angular-moviemasher-group"]

  default_action {
    target_group_arn = "${aws_alb_target_group.angular-moviemasher-group.arn}"
    type             = "forward"
  }
}

resource "aws_cloudwatch_log_group" "angular-moviemasher-logs" {
  name = "angular-moviemasher-logs"
}

data "template_file" "angular_moviemasher_task" {
  template = "${file("tasks/angular-moviemasher.json")}"

  vars {
    image                 = "${var.angular-moviemasher-image}"
    aws_access_key_id     = "${var.aws_access_key_id}"
    aws_secret_access_key = "${var.aws_secret_access_key}"
    s3_bucket             = "${var.s3_bucket}"
    s3_region             = "${var.s3_region}"
    user_media_protocol   = "${var.user_media_protocol}"
    user_media_host       = "${var.user_media_host}"
    sqs_queue_url         = "${var.sqs_queue_url}"
    aws_region            = "${var.aws_region}"
    log_group             = "${aws_cloudwatch_log_group.angular-moviemasher-logs.name}"
  }
}

resource "aws_ecs_task_definition" "angular-moviemasher" {
  family = "angular-moviemasher"
  container_definitions = "${data.template_file.angular_moviemasher_task.rendered}"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = "1024"
  memory                   = "2048"
  execution_role_arn       = "${aws_iam_role.ecs_service_role.arn}"
  task_role_arn            = "${aws_iam_role.ecs_host_role.arn}"
}

resource "aws_ecs_service" "angular-moviemasher" {
  name = "angular-moviemasher"
  cluster = "${aws_ecs_cluster.cluster.id}"
  task_definition = "${aws_ecs_task_definition.angular-moviemasher.arn}"
  desired_count = 1
  launch_type = "FARGATE"
  depends_on = ["aws_iam_role_policy.ecs_service_role_policy", "aws_iam_role_policy.ecr_service_role_policy", "aws_alb_target_group.angular-moviemasher-group"]
  network_configuration {
    security_groups = ["${aws_security_group.ecs.id}"]
    subnets         = ["${aws_subnet.angular_moviemasher_private_subnet.*.id}"]
  }
  load_balancer {
    target_group_arn = "${aws_alb_target_group.angular-moviemasher-group.arn}"
    container_name = "angular-moviemasher"
    container_port = 80
  }
}