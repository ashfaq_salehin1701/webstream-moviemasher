resource "aws_cloudwatch_log_group" "moviemasher-rb-autoscaling-logs" {
  name = "moviemasher-rb-autoscaling-logs"
}

data "template_file" "moviemasher_rb_autoscaling_task" {
  template = "${file("tasks/moviemasher-rb-single.json")}"

  vars {
    image                 = "${var.moviemasher-rb-image}"
    sqs_queue_url         = "${var.sqs_queue_url}"
    aws_access_key_id     = "${var.aws_access_key_id}"
    aws_secret_access_key = "${var.aws_secret_access_key}"
    aws_region            = "${var.aws_region}"
    log_group             = "${aws_cloudwatch_log_group.moviemasher-rb-logs.name}"
  }
}

resource "aws_ecs_task_definition" "moviemasher-rb-autoscaling" {
  family = "moviemasher-rb-autoscaling"
  container_definitions = "${data.template_file.moviemasher_rb_autoscaling_task.rendered}"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = "1024"
  memory                   = "2048"
  execution_role_arn       = "${aws_iam_role.ecs_service_role.arn}"
  task_role_arn            = "${aws_iam_role.ecs_host_role.arn}"
}

resource "aws_ecs_service" "moviemasher-rb-autoscaling" {
  name = "moviemasher-rb-autoscaling"
  cluster = "${aws_ecs_cluster.cluster.id}"
  task_definition = "${aws_ecs_task_definition.moviemasher-rb-autoscaling.arn}"
  launch_type = "FARGATE"
  depends_on = ["aws_iam_role_policy.ecs_service_role_policy", "aws_iam_role_policy.ecr_service_role_policy"]
  network_configuration {
    security_groups = ["${aws_security_group.ecs.id}"]
    subnets         = ["${aws_subnet.moviemasher_rb_autoscaling_private_subnet.*.id}"]
  }
  lifecycle {
    ignore_changes = ["desired_count"]
  }
}