import boto3

def lambda_handler(event, context):
    sqs = boto3.resource('sqs')
    queue = sqs.Queue(url='${queue_url}')
    approxMessages = int(queue.attributes.get('ApproximateNumberOfMessages'))
    if approxMessages > ${queue_high_threshold}:
        client = boto3.client('ecs')
        response = client.run_task(
            cluster = 'moviemasher-cluster',
            launchType = 'FARGATE',
            taskDefinition = '${task_definition}',
            count = 10 if (approxMessages - ${queue_high_threshold} > 10) else (approxMessages - ${queue_high_threshold}),
            platformVersion = 'LATEST',
            networkConfiguration = {
                'awsvpcConfiguration': {
                    'subnets': ['${subnet_id_1}', '${subnet_id_2}'],
                    'securityGroups': ['${security_group}'],
                    'assignPublicIp': 'DISABLED'
                }
            }
        )
        return str(response)
    else:
        return ""