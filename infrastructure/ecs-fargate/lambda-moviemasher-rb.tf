provider "archive" {}
provider "local" {}

data "template_file" "lambda_function" {
  template = "${file("lambda/launch-moviemasher-rb-tpl.py")}"

  vars {
    task_definition = "${aws_ecs_task_definition.moviemasher-rb-autoscaling.family}:${aws_ecs_task_definition.moviemasher-rb-autoscaling.revision}"
    subnet_id_1 = "${aws_subnet.moviemasher_rb_autoscaling_private_subnet.0.id}"
    subnet_id_2 = "${aws_subnet.moviemasher_rb_autoscaling_private_subnet.1.id}"
    security_group = "${aws_security_group.ecs.id}"
    queue_url = "${var.sqs_queue_url}"
    queue_high_threshold = "${var.queue_high_threshold}"
  }
}

resource "local_file" "launch-moviemasher-rb" {
  content = "${data.template_file.lambda_function.rendered}"
  filename = "lambda/libraries/launch-moviemasher-rb.py"
}

data "archive_file" "zip" {
  type        = "zip"
  output_path = "/tmp/launch-moviemasher-rb.zip"
  source_dir = "lambda/libraries"
  depends_on = ["local_file.launch-moviemasher-rb"]
}

data "aws_iam_policy_document" "policy" {
  statement {
    sid    = ""
    effect = "Allow"

    principals {
      identifiers = ["lambda.amazonaws.com", "ecs.amazonaws.com", "ecs-tasks.amazonaws.com"]
      type        = "Service"
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "iam_for_lambda" {
  name               = "iam_for_lambda"
  assume_role_policy = "${data.aws_iam_policy_document.policy.json}"
}

resource "aws_iam_role_policy" "lambda_role_policy" {
  name = "lambda_role_policy"
  policy = "${file("policies/lambda-role-policy.json")}"
  role = "${aws_iam_role.iam_for_lambda.id}"
}

#pip3 install boto3==1.4.8 -t libraries
resource "aws_lambda_function" "lambda_moviemasher_rb" {
  function_name = "moviemasher_rb_autoscaling"

  filename         = "${data.archive_file.zip.output_path}"
  source_code_hash = "${data.archive_file.zip.output_base64sha256}"

  role    = "${aws_iam_role.iam_for_lambda.arn}"
  handler = "launch-moviemasher-rb.lambda_handler"
  runtime = "python3.6"
}

resource "aws_lambda_permission" "cloudwatch_trigger" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.lambda_moviemasher_rb.arn}"
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.lambda.arn}"
}

resource "aws_cloudwatch_event_rule" "lambda" {
  name                = "lambda-moviemasher-rb-rule"
  description         = "Schedule trigger for lambda execution"
  schedule_expression = "rate(1 minute)"
}

resource "aws_cloudwatch_event_target" "lambda" {
  target_id = "lambda-moviemasher-rb-target"
  rule      = "${aws_cloudwatch_event_rule.lambda.name}"
  arn       = "${aws_lambda_function.lambda_moviemasher_rb.arn}"
}