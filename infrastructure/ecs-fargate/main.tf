# Specify the provider and access details
provider "aws" {
  region = "${var.aws_region}"
}

resource "aws_key_pair" "moviemasher_admin" {
  key_name = "${var.key_name}"
  public_key = "${file(var.public_key_path)}"
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
}

resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.main.id}"
}

## Angular moviemasher network resources
resource "aws_eip" "angular_moviemasher_nat_eip" {
  vpc        = true
  depends_on = ["aws_internet_gateway.main"]
}

resource "aws_nat_gateway" "angular_moviemasher_nat" {
  allocation_id = "${aws_eip.angular_moviemasher_nat_eip.id}"
  subnet_id     = "${element(aws_subnet.angular_moviemasher_public_subnet.*.id, 0)}"
  depends_on    = ["aws_internet_gateway.main"]
}

resource "aws_subnet" "angular_moviemasher_public_subnet" {
  vpc_id                  = "${aws_vpc.main.id}"
  count                   = "2"
  cidr_block              = "${element(var.angular_moviemasher_public_subnet_cidrs, count.index)}"
  availability_zone       = "${element(var.availability_zones, count.index)}"
  map_public_ip_on_launch = true
}

resource "aws_subnet" "angular_moviemasher_private_subnet" {
  vpc_id                  = "${aws_vpc.main.id}"
  count                   = "2"
  cidr_block              = "${element(var.angular_moviemasher_private_subnet_cidrs, count.index)}"
  availability_zone       = "${element(var.availability_zones, count.index)}"
  map_public_ip_on_launch = true
}

resource "aws_route_table" "angular_moviemasher_private" {
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_route_table" "angular_moviemasher_public" {
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_route" "angular_moviemasher_public_internet_gateway" {
  route_table_id         = "${aws_route_table.angular_moviemasher_public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.main.id}"
}

resource "aws_route" "angular_moviemasher_private_nat_gateway" {
  route_table_id         = "${aws_route_table.angular_moviemasher_private.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.angular_moviemasher_nat.id}"
}

resource "aws_route_table_association" "angular_moviemasher_public" {
  count          = "${length(var.angular_moviemasher_public_subnet_cidrs)}"
  subnet_id      = "${element(aws_subnet.angular_moviemasher_public_subnet.*.id, count.index)}"
  route_table_id = "${aws_route_table.angular_moviemasher_public.id}"
}

resource "aws_route_table_association" "angular_moviemasher_private" {
  count           = "${length(var.angular_moviemasher_private_subnet_cidrs)}"
  subnet_id       = "${element(aws_subnet.angular_moviemasher_private_subnet.*.id, count.index)}"
  route_table_id  = "${aws_route_table.angular_moviemasher_private.id}"
}

## Moviemasher rb network resources
resource "aws_subnet" "moviemasher_rb_public_subnet" {
  vpc_id                  = "${aws_vpc.main.id}"
  count                   = "2"
  cidr_block              = "${element(var.moviemasher_rb_public_subnet_cidrs, count.index)}"
  availability_zone       = "${element(var.availability_zones, count.index)}"
  map_public_ip_on_launch = true
}

resource "aws_subnet" "moviemasher_rb_private_subnet" {
  vpc_id                  = "${aws_vpc.main.id}"
  count                   = "2"
  cidr_block              = "${element(var.moviemasher_rb_private_subnet_cidrs, count.index)}"
  availability_zone       = "${element(var.availability_zones, count.index)}"
  map_public_ip_on_launch = true
}

resource "aws_route_table" "moviemasher_rb_private" {
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_route_table" "moviemasher_rb_public" {
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_route" "moviemasher_rb_public_internet_gateway" {
  route_table_id         = "${aws_route_table.moviemasher_rb_public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.main.id}"
}

resource "aws_route" "moviemasher_rb_private_nat_gateway" {
  route_table_id         = "${aws_route_table.moviemasher_rb_private.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.angular_moviemasher_nat.id}"
}

resource "aws_route_table_association" "moviemasher_rb_public" {
  count          = "${length(var.moviemasher_rb_public_subnet_cidrs)}"
  subnet_id      = "${element(aws_subnet.moviemasher_rb_public_subnet.*.id, count.index)}"
  route_table_id = "${aws_route_table.moviemasher_rb_public.id}"
}

resource "aws_route_table_association" "moviemasher_rb_private" {
  count           = "${length(var.moviemasher_rb_private_subnet_cidrs)}"
  subnet_id       = "${element(aws_subnet.moviemasher_rb_private_subnet.*.id, count.index)}"
  route_table_id  = "${aws_route_table.moviemasher_rb_private.id}"
}

# Moviemasher rb autoscaling network resources
resource "aws_subnet" "moviemasher_rb_autoscaling_public_subnet" {
  vpc_id                  = "${aws_vpc.main.id}"
  count                   = "2"
  cidr_block              = "${element(var.moviemasher_rb_autoscaling_public_subnet_cidrs, count.index)}"
  availability_zone       = "${element(var.availability_zones, count.index)}"
  map_public_ip_on_launch = true
}

resource "aws_subnet" "moviemasher_rb_autoscaling_private_subnet" {
  vpc_id                  = "${aws_vpc.main.id}"
  count                   = "2"
  cidr_block              = "${element(var.moviemasher_rb_autoscaling_private_subnet_cidrs, count.index)}"
  availability_zone       = "${element(var.availability_zones, count.index)}"
  map_public_ip_on_launch = true
}

resource "aws_route_table" "moviemasher_rb_autoscaling_private" {
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_route_table" "moviemasher_rb_autoscaling_public" {
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_route" "moviemasher_rb_autoscaling_public_internet_gateway" {
  route_table_id         = "${aws_route_table.moviemasher_rb_autoscaling_public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.main.id}"
}

resource "aws_route" "moviemasher_rb_autoscaling_private_nat_gateway" {
  route_table_id         = "${aws_route_table.moviemasher_rb_autoscaling_private.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.angular_moviemasher_nat.id}"
}

resource "aws_route_table_association" "moviemasher_rb_autoscaling_public" {
  count          = "${length(var.moviemasher_rb_autoscaling_public_subnet_cidrs)}"
  subnet_id      = "${element(aws_subnet.moviemasher_rb_autoscaling_public_subnet.*.id, count.index)}"
  route_table_id = "${aws_route_table.moviemasher_rb_autoscaling_public.id}"
}

resource "aws_route_table_association" "moviemasher_rb_autoscaling_private" {
  count           = "${length(var.moviemasher_rb_autoscaling_private_subnet_cidrs)}"
  subnet_id       = "${element(aws_subnet.moviemasher_rb_autoscaling_private_subnet.*.id, count.index)}"
  route_table_id  = "${aws_route_table.moviemasher_rb_autoscaling_private.id}"
}

resource "aws_security_group" "load_balancers" {
  name = "load_balancers"
  description = "Allows all traffic"
  vpc_id = "${aws_vpc.main.id}"

  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "ecs" {
  name = "ecs"
  description = "Allows all traffic"
  vpc_id = "${aws_vpc.main.id}"

  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    security_groups = ["${aws_security_group.load_balancers.id}"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_ecs_cluster" "cluster" {
  name = "moviemasher-cluster"
}

resource "aws_iam_role" "ecs_host_role" {
  name = "ecs_host_role"
  assume_role_policy = "${file("policies/ecs-role.json")}"
}

resource "aws_iam_role_policy" "ecs_instance_role_policy" {
  name = "ecs_instance_role_policy"
  policy = "${file("policies/ecs-instance-role-policy.json")}"
  role = "${aws_iam_role.ecs_host_role.id}"
}

resource "aws_iam_role" "ecs_service_role" {
  name = "ecs_service_role"
  assume_role_policy = "${file("policies/ecs-role.json")}"
}

resource "aws_iam_role_policy" "ecs_service_role_policy" {
  name = "ecs_service_role_policy"
  policy = "${file("policies/ecs-service-role-policy.json")}"
  role = "${aws_iam_role.ecs_service_role.id}"
}

resource "aws_iam_role_policy" "ecr_service_role_policy" {
  name = "ecr_service_role_policy"
  policy = "${file("policies/ecr-service-role-policy.json")}"
  role = "${aws_iam_role.ecs_service_role.id}"
}

resource "aws_iam_instance_profile" "ecs" {
  name = "ecs-instance-profile"
  path = "/"
  role = "${aws_iam_role.ecs_host_role.name}"
}
