# Specify the provider and access details
provider "aws" {
  region  = "${var.aws_region}"
  profile = "${var.key_name}"
}

resource "aws_sqs_queue" "moviemasher_queue" {
  name                        = "moviemasher-queue"
}
