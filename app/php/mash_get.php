<?php

if (! @include_once(dirname(__FILE__) . '/include/loadutils.php')) $err = 'Problem loading utility script';
if ((! $err) && (! load_utils('api','data', 'auth'))) $err = 'Problem loading utility scripts';

$response = array();

if (! $err) { // pull in configuration so we can log other errors
  $config = config_get();
  $err = config_error($config);
  $log_responses = $config['log_response'];
}
if (! $err) { // see if the user is authenticated (does not redirect or exit)
  if (! auth_ok()) $err = 'Unauthenticated access';
}

if (! $err) {
  $userid = auth_userid();
  $media_file_json_path = get_media_file_json_path('video', $userid, $config);
  $media_file_json_content = file_get_contents($media_file_json_path);
  $medias = json_decode($media_file_json_content, true);
  $job_id = data_combine_videos($userid, $medias, $config);
  $mash = data_mash($job_id, $userid, $config);
  if (!$mash) $err = 'Could not find mash ' . $job_id;
  if (!$err) {
    $response['mash'] = $mash;
  }
}
if ($err) $response['error'] = $err;
$json = json_encode($response);
print $json . "\n\n";
