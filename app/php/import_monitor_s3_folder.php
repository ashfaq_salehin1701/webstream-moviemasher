<?php

$log_responses = '';
$err = '';
$config = array();
$response = array();
$files = array();
$s3_path = array();

if (! @include_once(dirname(__FILE__) . '/include/loadutils.php')) $err = 'Problem loading utility script';
if ((! $err) && (! load_utils('api','data', 'auth'))) $err = 'Problem loading utility scripts';

if (! $err) { // pull in configuration so we can log other errors
  $config = config_get();
  $err = config_error($config);
  $log_responses = $config['log_response'];
}
if (! $err) { // see if the user is authenticated (does not redirect or exit)
  if (! auth_ok()) $err = 'Unauthenticated access';
}
$userid = auth_userid();
if (! $err) { // pull in other configuration and check for required input
  if (! $php_input = file_get_contents('php://input')) $err = 'JSON payload required';
  else if (! $request = @json_decode($php_input, TRUE)) $err = 'Could not parse JSON payload';
}

$all_jobs_processed = true;
if (! $err) {
  $files = $request['files'];
  $s3_path = $request['s3_path'];
  $pending_jobs = 0;
  foreach ($files as $i => $job) {
    if ((!array_key_exists('finished', $files[$i])) || ($job['finished'] != 1)) {
      $job_id = $job['id'];
      $progress_file = path_concat($config['temporary_directory'], $job_id . '.json');
      $json_str = file_get($progress_file);
      if (!$json_str) { // no progress file created yet
        $files[$i]['completed'] = 0.01;
        $files[$i]['status'] = 'Queued';
      } else {
        if ($config['log_api_response']) log_file($json_str, $config);
        $json_object = @json_decode($json_str, TRUE);
        if (empty($json_object['source'])) {
          $json_object['source'] = $job['source'];
          file_put_contents(path_concat($config['temporary_directory'], $job_id . '.json'), json_encode($json_object));
        }
        if (!is_array($json_object)) $err = 'Could not parse response';
        if (!$err) {
          if (!empty($json_object['error'])) $err = $json_object['error'];
        }
        if (!$err) {
          if (!empty($json_object['id'])) {
            $media_data = api_import_data($json_object, $config);
            if (!empty($media_data['error'])) $err = $media_data['error'];
            else $err = data_save_media($media_data, $json_object['uid'], $config);
            $files[$i]['completed'] = 1;
          } else {
            if (empty($json_object['completed'])) {
              $json_object['completed'] = 0;
              if (!empty($json_object['progress'])) {
                $json_object['completed'] = api_progress_completed($json_object['progress']);
              }
            }
            if (!$json_object['completed']) $err = 'Could not find progress or completed in response';
            else {
              $files[$i]['completed'] = $json_object['completed'];
              if (empty($json_object['status'])) $json_object['status'] = floor($json_object['completed'] * 100) . '%';
              $files[$i]['status'] = $json_object['status'];
            }
          }
          $done = (1 == $files[$i]['completed']);
          if ($done || !empty($err)) {
            $files[$i]['finished'] = 1;
          }
          if ($done) {
            $files[$i]['type'] = (empty($json_object['type']) ? '' : $json_object['type']);
            if ('video' == $files[$i]['type']) {// check to make sure we actualy got visuals
              if (!(empty($json_object['no_video']) || ('false' == $json_object['no_video']))) $files[$i]['type'] = 'audio';
            }
          }
        }
      }
      $all_jobs_processed &= (array_key_exists('finished', $files[$i]) && ($files[$i]['finished'] == 1));
    }
  }
  if ($all_jobs_processed) {
    foreach ($files as $i => $job) {
      $job_id = $job['id'];
      $progress_file = path_concat($config['temporary_directory'], $job_id . '.json');
      unlink($progress_file);
    }
    change_source('video', $files, $userid, $config);
    $media_file_json_path = get_media_file_json_path('video', $userid, $config);
    $media_file_json_content = file_get_contents($media_file_json_path);
    $medias = json_decode($media_file_json_content, true);
    $job_id = data_combine_videos($userid, $medias, $config);
    $mash = data_mash($job_id, $userid, $config);
    if (! $mash) $err = 'Could not find mash ' . $job_id;
    if (! $err) {
      $response['mash'] = $mash;
      $result = api_export(array('id' => $job_id, 'mash' => $mash), array('include_progress' => 1), $config);
      if (! empty($result['error'])) $err = $result['error'];
      else $response['merge_job_id'] = $result['id'];
    }
  }
  if ($err) $response['error'] = $err;
  else $response['jobs'] = $files;
  $json = json_encode($response);
  print $json . "\n\n";
  if ($log_responses) log_file($json, $config);
}
