<?php

$response = array();
$err = '';
$config = array();

if (! @include_once(dirname(__FILE__) . '/include/loadutils.php')) $err = 'Problem loading utility script';
if ((! $err) && (! load_utils('auth','service'))) $err = 'Problem loading utility scripts';

if (! $err) { // pull in configuration so we can log other errors
  $config = config_get();
  $err = config_error($config);
  $log_responses = $config['log_response'];
}
// autheticate the user (will exit if not possible)
if ((! $err) && (! auth_ok())) auth_challenge($config);

if (! $err) { // pull in configuration so we can log other errors
  $config = config_get();
  $err = config_error($config);
  $log_responses = $config['log_response'];
}
// autheticate the user (will exit if not possible)
if ((! $err) && (! auth_ok())) auth_challenge($config);

if (! $err) { // pull in other configuration and check for required input
  if (! $php_input = file_get_contents('php://input')) $err = 'JSON payload required';
  else if (! $request = @json_decode($php_input, TRUE)) $err = 'Could not parse JSON payload';
}

if (! $err) {
  $s3_path = (empty($request['s3_path']) ? '' : $request['s3_path']);
  if (! $s3_path ) $err = 'Parameter s3_path required';
}

if (! $err) {
  $uid = auth_userid();
  $response['path'] = s3_folder_import($s3_path, $config);
}

if ($err) $response['error'] = $err;
else $response['ok'] = 1;

$json = json_encode($response);
print $json . "\n\n";
if ($log_responses) log_file($json, $config);
